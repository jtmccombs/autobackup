<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;

class BackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:backup {--clean}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup the sites';

    protected $sites;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->sites = [];
        $domains = explode(',', env('BACKUP_DOMAINS'));
        $databases = explode(',', env('BACKUP_DATABASES'));
        $paths = explode(',', env('BACKUP_PATHS'));
        foreach($domains as $idx => $domain) {
            $db = $databases[$idx];
            $path = $paths[$idx];
            $this->sites[] = [
                'domain' => $domain,
                'database' => $db,
                'paths' => [$path],
            ];
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->sites as $site) {
            $this->setupConfigForSite($site);
            $this->callBackupCommand();
        };
    }

    protected function setupConfigForSite($site)
    {
        Config::set('laravel-backup.backup.name', $this->siteName($site));
        Config::set('database.connections.mysql.database', $site['database']);
        Config::set('laravel-backup.backup.source.files.include', $this->siteIncludes($site));
    }

    protected function siteName($site)
    {
        return 'https://'.$site['domain'];
    }

    protected function siteIncludes($site)
    {
        return array_map(function ($path) use ($site) {
            return base_path("../$path");
        }, $site['paths']);
    }

    protected function callBackupCommand()
    {
        $this->call('backup:'.$this->backupType());
    }

    protected function backupType()
    {
        return $this->option('clean') ? 'clean' : 'run';
    }
}
